# python4microbiodata2021_practicalsessions

## Dados do dia 1

 * [Riva et al. A fiber-deprived diet disturbs the fine-scale spatial architecture of the murine colon microbiome. (2019)](https://www.nature.com/articles/s41467-019-12413-0)

## Dados do dia 2

 * [Santos et al. Genomic and Phenotypic Heterogeneity of Clinical Isolates of the Human Pathogens Aspergillus fumigatus, Aspergillus lentulus, and Aspergillus fumigatiaffinis (2020)](https://www.frontiersin.org/articles/10.3389/fgene.2020.00459/full)

## Dados do dia 3

 * [Meirelles et al. Bacterial defenses against a natural antibiotic promote collateral resilience to clinical antibiotics (2021)](https://journals.plos.org/plosbiology/article?id=10.1371/journal.pbio.3001093)
